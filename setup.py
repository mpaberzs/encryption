from setuptools import setup, find_packages
setup(
    name="encryption",
    version="1.1.0",
    packages=['encryption'],
    install_requires=['cryptography==2.3.1']
)
